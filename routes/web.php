<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/biodata', 'AuthController@biodata');
Route::post('/send', 'AuthController@send');

Route::get('/master', function () {
    return view('layout.master');
});

Route::get('/data-tables', 'IndexController@table');

Route::group(['middleware' => ['auth']], function () {
    //CRUD cast
    //Create
    Route::get('/cast/create', 'CastController@create'); //go to add data form
    Route::post('/cast', 'CastController@store'); //save data form to database

    //Read
    Route::get('/cast', 'CastController@index'); //take data from database then show to blade
    Route::get('/cast/{cast_id}', 'CastController@show'); //show cast detail data based on id

    //Update
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //go to edit form
    Route::put('/cast/{cast_id}', 'CastController@update'); //save edit data to database

    //Delete
    Route::delete('/cast/{cast_id}', 'CastController@destroy'); //delete cast data based on id

    //Profile Index, Update
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);
});

//CRUD Berita
Route::resource('berita', 'BeritaController');

//Auth
Auth::routes();