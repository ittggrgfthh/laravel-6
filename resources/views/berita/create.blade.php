@extends('layout.master')

@section('title')
    Halaman Create Berita
@endsection

@section('content')
<form method="POST" action="/berita" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Berita</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Content</label>
        <textarea name="content" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputPassword1">Cast</label>
        <select name="cast_id" class="form-control" id="">
            <option value="">--- Pilih Cast ---</option>
            @foreach ($cast as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                
            @endforeach
        </select>
    </div>
    @error('cast_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label for="exampleInputPassword1">Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
