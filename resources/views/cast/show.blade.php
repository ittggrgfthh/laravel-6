@extends('layout.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('content')
<h1>{{ $cast->nama }}</h1>
<p>{{ $cast->umur }}</p>
<p>{{ $cast->bio }}</p>

<div class="row">
    @foreach ($cast->berita as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{ asset('gambar/' . $item->thumbnail) }}" class="card-img-top" alt="...">
            <div class="card-body">
                <h2 class="card-title"><b>{{ $item->judul }}</b></h2>
                <span class="badge badge-info">{{ $item->cast->nama }}</span>
                <p class="card-text">{{ Str::limit($item->content, 30) }}</p>
    
                @auth
                    <form action="/berita/{{ $item->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/berita/{{ $item->id }}" class=" btn btn-info btn-sm">Detail</a>
                        <a href="/berita/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>    
                @endauth

                @guest
                    <a href="/berita/{{ $item->id }}" class=" btn btn-info btn-sm">Detail</a>
                @endguest
                
            </div>
          </div>
    </div>
    @endforeach
</div>
@endsection