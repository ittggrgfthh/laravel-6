@extends('layout.master')

@section('title')
    Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{ $name }}</h1>
    <p><b>Terima kasih telah bergabung di Website kami. Media Belajar kita bersama!</b></p>
@endsection